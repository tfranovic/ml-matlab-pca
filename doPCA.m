function [ z, Vl, a ] = doPCA(x,R,type,s)
%DOPCA Perform PCA on data using specified kernel
if strcmp(type,'linear')
    k=@k_lin;
    s=0;
else
    k=@k_gau;
end
N=size(x,1);
K=getK(x,k,s);
Kt=K-(ones(N)/N)*K-K*(ones(N)/N)+(ones(N)/N)*K*(ones(N)/N);
[A,a]=eigs(Kt,R,'lm');
A'*A
a=diag(a);
Vl=zeros(N,R);
for i=1:R
    Vl(:,i)=A(:,i)/sqrt(a(i));
end
z=Kt*Vl;
end
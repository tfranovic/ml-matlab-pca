function [] = plotPCA(z,y)
classes=unique(y)';
classes=sort(classes);
colors=['r','g','b'];
figure();
hold on;
for c=classes
    scatter(z(y==c,1),z(y==c,2), colors(c+1), 'filled');
end
axis square;
end


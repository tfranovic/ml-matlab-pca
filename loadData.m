clear all;
x=load('usps_012_X.txt');
y=load('usps_012_y.txt');
N=size(x,1);
mu = mean(x); sigma = std(x);
x = (x - repmat(mu, N, 1)) ./ repmat(sigma, N, 1);
clear N mu sigma ans;
function [ k ] = k_gau(x1, x2, s)
%K_GAU Gaussian kernel function
k=x1-x2;
k=-sum(k.^2)/(s^2);
k=exp(k);
end


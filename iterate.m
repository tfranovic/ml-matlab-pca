clear all;
loadData;

%linear
z=doPCA(x,2,'linear');
plotPCA(z,y);

%gaussian
S=[16, 32, 64, 128];
for s=S
    z=doPCA(x,2,'gaussian',s);
    plotPCA(z,y);
end